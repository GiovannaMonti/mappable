create table language(
language_id varchar primary key
);

create table genre(
genre_id varchar primary key
);

create table place(
latitude float not null,
longitude float not null,
place_name varchar primary key,
country varchar,
unique(latitude, longitude)
);

create table work (
title varchar primary key,
incipit varchar,
"date" date not null,
rhyme_scheme varchar,
syllabic_pattern varchar,
metric_repertoire varchar,
body text,
language varchar references language(language_id) on update cascade,
genre varchar references genre(genre_id) on update cascade,
place_of_composition varchar references place(place_name) on update cascade
);

create table author(
name varchar primary key,
birth_date date,
death_date date,
senhal varchar,
body text
);

create table author_language(
name varchar references author(name) on update cascade on delete cascade,
language_id varchar references language(language_id) on update cascade,
primary key(name, language_id)
);

create table authorship(
name varchar references author(name) on update cascade on delete cascade,
title varchar references work(title) on update cascade on delete cascade,
primary key(name, title)
);

create table source(
source_id varchar primary key,
type varchar not null,
pressmark varchar,
"sign" varchar,
source_date date not null,
body text
);

create table derivation(
title varchar references work(title) on update cascade on delete cascade,
source_id varchar references source(source_id) on update cascade on delete cascade,
primary key(title, source_id)
);

create table source_genre(
source_id varchar references source(source_id) on update cascade on delete cascade,
genre_id varchar references genre(genre_id) on update cascade,
primary key(source_id, genre_id)
);

create table source_language(
source_id varchar references source(source_id) on update cascade on delete cascade,
language_id varchar references language(language_id) on update cascade,
primary key(source_id, language_id)
);

create table production(
source_id varchar references source(source_id) on update cascade on delete cascade,
place_id varchar references place(place_name) on update cascade,
primary key(source_id, place_id)
);


alter table work
add constraint title_incipit_notnull
check(
   not(
     (title is null or title = '')
     and
     (incipit is null or incipit = '')
   )
);


insert into language(language_id) values ('french');
insert into language(language_id) values ('italian');
insert into language(language_id) values ('occitan');
insert into language(language_id) values ('galician-portuguese');
insert into language(language_id) values ('german');

insert into genre(genre_id) values ('novel');
insert into genre(genre_id) values ('chanson de geste');
insert into genre(genre_id) values ('canzone');
insert into genre(genre_id) values ('tenzone');
insert into genre(genre_id) values ('lais');
insert into genre(genre_id) values ('descort');
insert into genre(genre_id) values ('lyric');

insert into place values (36.849998, 14.766667, 'Modica', 'Italy');
insert into place values (41.683334, 15.383333, 'San Severo', 'Italy');
insert into place values (43.318611, 11.330556, 'Siena', 'Italy');
insert into place values (39.083332, 17.116667, 'Crotone', 'Italy');
insert into place values (44.836151, -0.580816, 'Bordeaux', 'France');
insert into place values (47.243599, 0.689200, 'Tours', 'France');
insert into place values (45.763420, 4.834277, 'Lyon', 'France');

insert into work values ('Divina Commedia', 'spbfwobv', '31/12/1200', 'xxx', 'xxx', 'xxx', 'jsbcjka', 'italian', 'lyric', 'Siena');
insert into work values ('Pippo', 'spbfwobv', '31/12/1305', 'xxx', 'xxx', 'xxx', 'jsbcjka', 'italian', 'lyric', 'Modica');
insert into work values ('Pluto', 'spbfwobv', '31/12/1450', 'xxx', 'xxx', 'xxx', 'jsbcjka', 'occitan', 'lais', 'Lyon');
insert into work values ('Lime', 'spbfwobv', '31/12/1215', 'xxx', 'xxx', 'xxx', 'jsbcjka', 'french', 'descort', 'Tours');

insert into author values ('Dante Alighieri', '20/10/1170', '13/07/1245');
insert into author values ('Paperino', '20/10/1270', '13/07/1349');
insert into author values ('Topolino', '20/10/1405', '13/07/1479');
insert into author values ('Amelia', '20/10/1184', '13/07/1238');

insert into author_language values ('Dante Alighieri', 'italian');
insert into author_language values ('Paperino', 'italian');
insert into author_language values ('Topolino', 'occitan');
insert into author_language values ('Amelia', 'french');

insert into authorship values ('Dante Alighieri', 'Divina Commedia');
insert into authorship values ('Paperino', 'Pippo');
insert into authorship values ('Topolino', 'Pluto');
insert into authorship values ('Amelia', 'Lime');
