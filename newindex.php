<!DOCTYPE html>
<html lang="it">
<?php
    $dbconn = pg_connect("dbname=tesisemplificato user=postgres password=unimi") or die('Connection Failed'); ?>
<head>
  <meta charset="UTF-8">
  <title>Home</title>
  <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
  integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
  crossorigin=""/>
  <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
  integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
  crossorigin=""></script>
  <script src="js/leaflet-categorized-layers.js"></script>
  <link rel="stylesheet" href="css/leaflet-categorized-layers.css">
<!--  <link rel="stylesheet" href="leaflet/leaflet.css"/>
  <script src="leaflet/leaflet.js"></script> -->
</head>
<body>

  <!-- NAVBAR -->
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand" href="newindex.php">Mappable</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav"> <!-- elementi della navbar che vengono collassati nell'ham menu -->
        <ul class="navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Contatti</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- END NAVBAR -->

  <div class="container">
    <div id="mapid">
    </div>
  </div>


  <script src="js/map.js"></script>

  <?php
  $query1 = pg_query("SELECT * FROM place JOIN work ON place_name=place_of_composition ORDER BY country");?>

  <script type="text/javascript"> var popupinfo = []; </script>
  <?php
  while ($riga1=pg_fetch_assoc($query1)){
    $latitude=$riga1['latitude'];
    $longitude=$riga1['longitude'];
    $place=$riga1['place_name'];
    $country=$riga1['country'];
    $work=$riga1['title'];
    echo "<script type=\"text/javascript\">
        info = {
        title: '$work',
        place_of_composition: '$place',
        latitude : $latitude,
        longitude : $longitude,
        country : '$country'
      }
      popupinfo.push(info);
    </script>
   ";
  }
  ?>
  <script type="text/javascript">

  //funzione per raggruppare qualcosa in base alla sua proprietà
    function groupByCountry(infoarray, property){
      let k= [...infoarray];
      return k.reduce((acc, obj) => { //prende ogni oggetto dell'array e guarda il paese,
        //per ogni paese accumula tutti gli oggetti con quel paese
        const key= obj[property]; //il paese
        if (!acc[key]){
          acc[key]=[];
        }
        acc[key].push(obj);
        return acc;
      }, {});
    }
//uso della funzione
    const worksGroupedByCountry = groupByCountry(popupinfo, 'country');
    //è un OGGETTO del tipo {France: [array di oggetti], Italy: [array di oggetti]}
    console.log(worksGroupedByCountry);

    function getInfoFrom(object) {
      var popupitems = [];
      for (var key in object) {
        if (object.hasOwnProperty(key)) {
          var stringLine = key + " : " + object[key];
          popupitems.push(stringLine);
        }
      }
      return popupitems;

    }


//funzione per creare array separati dalle operazioni precedenti
    function separateGroups(groupedinfo){
      let k= groupedinfo;
      let overlayMaps = {};
      for (var key in k){
        var singlegroup=k[key];
        var markers = [];
        for(var j=0; j<singlegroup.length; j++){
          var popupcontent = getInfoFrom(singlegroup[j]).join('<br/>');
          var showid='showmore'.concat(j);
          var html='<br/> <button type="button" id="'+showid+'" class="btn btn-sm" data-toggle="modal" data-target="#showmoremodal">Show more</button>';
          popupcontent=popupcontent.concat(html);
          var marker = L.marker([singlegroup[j].latitude, singlegroup[j].longitude]);
          marker.bindPopup(popupcontent);
          markers.push(marker);
        }
         placelayer = L.layerGroup(markers);
         overlayMaps[[key]]= placelayer; //usando [key] prendo il valore della variabile key
      }
      L.control.layers({}, overlayMaps).addTo(mymap);
    }
    separateGroups(worksGroupedByCountry);

  /*
  function filterByCountry(){
    let k= [...popupinfo];
    let filtervalue = document.getElementById('selectprova').value;
    let max=k.length;
    for( var i=0; i<max; i++){
      if(k[i].country!=filtervalue){
        k[i]=null;
      }
    }
    var filtered = k.filter(function (el) {
    return el != null;
    });
    return filtered;
  }

  var placelayer=null;

  function printInfo(){
    if(placelayer!=null){
      mymap.removeLayer(placelayer);
    }
    let filtervalue = document.getElementById('selectprova').value;
    if(filtervalue=='all') {
      showAllMarkers();
    }
    else{
      var filteredinfo= filterByCountry();
      var markerslayer = [];

      for(var j=0; j<filteredinfo.length; j++){
        var popupcontent = getInfoFrom(filteredinfo[j]).join('<br/>');
        var showid='showmore'.concat(j);
        var html='<br/> <button type="button" id="'+showid+'" class="btn btn-sm" data-toggle="modal" data-target="#showmoremodal">Show more</button>';
        popupcontent=popupcontent.concat(html);
        var marker = L.marker([filteredinfo[j].latitude, filteredinfo[j].longitude]);
        marker.bindPopup(popupcontent);
        marker.on('click', onClick);
        markerslayer.push(marker);
      }
       placelayer = L.layerGroup(markerslayer);
       placelayer.addTo(mymap);
    }
  }
  function onClick(e) {
   var popup = e.target.getPopup();
   var content = popup.getContent();
   console.log(content);
}

  function showAllMarkers(){
      var markerslayer = [];
      for(var j=0; j<popupinfo.length; j++){
        var popupcontent = getInfoFrom(popupinfo[j]).join('<br/>');
        var showid='showmore'.concat(j);
        var html='<br/> <button type="button" id="'+showid+'" class="btn btn-sm" data-toggle="modal" data-target="#showmoremodal">Show more</button>';
        popupcontent=popupcontent.concat(html);
        var marker = L.marker([popupinfo[j].latitude, popupinfo[j].longitude]);
        marker.bindPopup(popupcontent);
        marker.on('click', onClick);
        markerslayer.push(marker);
       placelayer = L.layerGroup(markerslayer);
       placelayer.addTo(mymap);
    }
  }

  function showMore(){

  }
*/
  </script>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="js/bootstrap/bootstrap.min.js"></script>

</body>
</html>
